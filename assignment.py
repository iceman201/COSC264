#*****************************************#
#        COSC 264 Group_Assignment        #
#**************#************#*************#
#Student name: # Liguo Jiao # Weiheng Xie #
#**************#************#*************#
#Student ID:   #  91734390  #  39283712   #
#**************#************#*************#

import numpy as np
import scipy.misc as sp
from random import *

def t_max(n, u):
    #Hamming bound
    overhead = 100; i = 0; total = 0;
    k = overhead + u
    result = 2 ** (n - k)    
    while (total < result):
        total += sp.comb(n,i,exact=True)
        i += 1
        if (total == result):
            return i - 1
    return i - 2

def error_distribution(u, p):
    # creating random numbers of errors
    return np.random.binomial(u, p, 1)[0]

def building_empty(size):
    # create a amount of empty block list 
    list1 = []
    for i in range(size):
        list1.append(0)
    return list1

def average_efficiency(t, u, n):
    # calculate the average efficiency   
    i = 0
    while (i < 1000000):
        t[i] = u / (t[i] * n)
        # u for user_data
        # t for trials
        # n for the package_length
        i += 1
    average = sum(t) / 1000000
    return average

if __name__ == "__main__":
    user_data = int(input("Please specify values for user data size(u): "))
    redundant_bits = int(input("Size of redundant bits(n-k): "))
    bit_error_probability = float(input("Bit error probability(p): "))
    package_length = user_data + redundant_bits + 100  #100 for Overhead
    packets = 0
    trials = building_empty(1000000)
    i = t_max(package_length, user_data)
    
    while (packets < 1000000):
        y = error_distribution(package_length, bit_error_probability)
        trials[packets] += 1
        while (y > i):
            y = error_distribution(package_length, bit_error_probability)
            trials[packets] += 1
        packets += 1
        # when the packets transimited sucessful
        # packets += 1 move to next package
    print(average_efficiency(trials, user_data, package_length))
